#Breakout Brew - In Development
###Made in Unity3D 2017.1.0p4
###Vertical slice (latest major build)

#Team: Basically Witchcraft

###Artists
	Brody Nolan
	Megan Tyler
	Mitchell Wilkinson
	
###Designers
	Daimen Paquin-Nault
	Jacob Foster
	Michelle Pollak
	
###Programmers
	David Horntvedt
	Rob Gentile
	